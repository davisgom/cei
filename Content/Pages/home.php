<section class="homepage-intro  text-lg-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-8">
        <h2 class="intro-text">
          <b class="text-1">Industry</b>
          <span>+</span>
          <b class="text-2">University</b>
        </h2>

        <br />

        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur totam at quia, numquam autem quibusdam eum molestias ut aliquid itaque libero obcaecati corporis laborum eveniet molestiae minus quo illum cumque!
        </p>

        <a href="about" class="btn btn-theme btn-theme-accent">
          Learn More
        </a>
      </div>
    </div>
  </div>
</section>


<!-- **** FEATURE AND NEWS SECTION **** -->
<section class="feature-and-news">
  <div class="container">
    <div class="row">
      <div class="feature col-12 col-lg-6 col-xl-8">
        
        <div class="container">

          <!-- **** EXAMPLE FOR FEATURED TEXT ITEM **** -->
          <!-- <div class="row">
            <div class="col-xl-4 mb-3">
              <img src="Content/Images/placeholder-image.jpg" class="img-fluid" />
            </div>

            <div class="col-12 col-xl-8">
              <h2>Title of this featured item</h2>
              <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit sint nobis velit dolore assumenda cumque odit illo! Cum corporis, quidem quasi eum.
              </p>
              <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed ">Learn More</a>
            </div>
          </div> -->

          <!-- **** EXAMPLE FOR VIDEO ITEM **** -->
          <div class="row">
            <div class="col-12">
              <div class="embed-responsive embed-responsive-16by9">
              
                <video id="placeholder-video" class="embed-responsive-item video-js" controls="" preload="metadata" title="Placeholder Video" style="background-color: transparent;" data-setup="{}" poster="/Content/Images/cei-video-placeholder-poster.jpg">
                  <source src="/Content/Videos/city-video-placeholder.mp4" type="video/mp4">
                </video>
              </div>
            </div>
          </div>
        </div>

      </div>
      
      <div class="news col-12 col-lg-6 col-xl-4">
        
        <div class="container">
          <div class="row">
            <div class="col">
              <h2>Recent Publications</h2>

              <!-- **** IMPORTANT: FOR DESIGN PLEASE ONLY DISPLAY THREE ITEMS!! **** -->

              <article>
                <h3><a href="">Strategies and Tools for Economic Development Organizations to Champion Equitable Economic Development</a></h3>
              </article>

              <article>
                <h3><a href="">Making ends meet: women's social capital development in regional informal economies</a></h3>
              </article>

              <article>
                <h3><a href="">Energy challenges in the Keweenaw: recruiting the community to develop innovative, acceptable solutions and local supply chains</a></h3>
              </article>

              <br />

              <a href="#" class="btn btn-sm btn-outline-secondary">
                View all Publications
              </a>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div> 
</section>


<!-- **** SOCIAL MEDIA BUTTONS SECTION **** -->

<?php include("Views/Shared/Partials/social.php"); ?>


<!-- **** FEATURED PROGRAMS SECTION **** -->

<section class="programs">
  <div class="container">
    <div class="row">
      <h2>Featured</h2>

  <div class="row">
      <div class="col-md-12">
      
              <ul>
                  <li class="item">
                      <span>
                      <div class="rei"></div>
                       <h3>Faculty Seminar Series</h3>                      
                      <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam ad consequatur officiis excepturi dolor facere odio reprehenderit, a pariatur totam repellat voluptatem repudiandae, nostrum soluta quae nobis autem natus eaque.
                      </p>
                      <a class="btn btn-theme btn-theme-secondary" href="#">Learn More</a>
                      </span>
                  </li>
                  
                  <li class="item">
                      <span>
                      <div class="cf"></div>
                    <h3> Faculty Profiles </h3>
                      <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam ad consequatur officiis excepturi dolor facere odio reprehenderit, a pariatur totam repellat voluptatem repudiandae, nostrum soluta quae nobis autem natus eaque.
                      </p>
                      <a class="btn btn-theme btn-theme-secondary" href="#">Learn More</a>
                      </span>
                  </li>
                  
                  <li class="item">
                      <span> 
                      <div class="cp"></div>
                      <h3> Events </h3>
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam ad consequatur officiis excepturi dolor facere odio reprehenderit, a pariatur totam repellat voluptatem repudiandae, nostrum soluta quae nobis autem natus eaque.
                      </p>
                      <a class="btn btn-theme btn-theme-secondary" href="#">Learn More</a>
                      </span>
                  </li>
              </ul>

      </div>
  </div>
    </div>
  </div>  
</section>

<!-- **** LINKS TO OTHER SITES **** -->

<section class="ced-sites">

<div class="container">   
    <div class="row mb-4">
      <div class="col-12 d-flex justify-content-center">    
        
        <!-- <small>CCED Sites</small> -->
        
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-12 d-flex justify-content-center">    
        
        <a href="#" class="text-hide site cced-logo-color">
          <?php echo $cced; ?>
        </a>

        <span class="d-block"></span>

        <a href="#" class="text-hide site rei-logo-color">
          MSU EDA University Center for Regional Economic Innovation
        </a>

        <span class="d-block"></span>

        <a href="#" class="text-hide site domicology-logo-color">
          Domicology
        </a>

      </div>
    </div>
  </div>
</section>
