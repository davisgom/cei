<?php $page_title="Circular Economies Institute Panel: \n Lorem ipsum dolor sit amet consectetur" ?>


<section class="row">
  <div class="col-12">
    <div class="embed-responsive embed-responsive-16by9">
      <video id="placeholder-video" class="embed-responsive-item video-js" controls="" preload="metadata" title="Placeholder Video" style="background-color: transparent;" data-setup="{}" poster="/Content/Images/cei-video-placeholder-poster.jpg">
        <source src="/Content/Videos/city-video-placeholder.mp4" type="video/mp4">
      </video>
    </div>
  </div>
</section>

<hr class="my-5" />

<section class="content">

  <p>
    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repudiandae eaque, sed hic recusandae, blanditiis laboriosam quia eius adipisci nihil fugiat ea excepturi nam mollitia quisquam odio iure magni sunt atque. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repudiandae eaque, sed hic recusandae, blanditiis laboriosam quia eius adipisci nihil fugiat ea excepturi nam mollitia quisquam odio iure magni sunt atque. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repudiandae eaque, sed hic recusandae, blanditiis laboriosam quia eius adipisci nihil fugiat ea excepturi nam mollitia quisquam odio iure magni sunt atque.
  </p>

  <a href="#" class="btn btn-theme btn-theme-primary">
    Download Transcript
  </a>

</section>