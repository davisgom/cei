<!-- <ul id="tasks">

            <?php


              $data = file("https://docs.google.com/spreadsheets/d/e/2PACX-1vTWCmU51VYkoFWF6cRexmo-YFr9mwrZYU7-Z-TG23dOuFKwznmPZL649Eb6TLWBaFqBEwaL7wa74XU3/pub?gid=928002392&single=true&output=tsv");
              foreach ($data as $task) {
                $taskArray = explode("\t", $task);
                list($name, $status, $desc, $notes, $dept, $tag) = $taskArray;
                print_r('<li class="task ' . $status . '" >');
                  print_r('<h2>' . $name . '</h2>');

                  print_r('<p>' . $desc . '</p>');
                  print_r('<p>' . $notes . '</p>');

                  print_r('<div class="tags">');
                    print_r('<i>' . $dept . '</i>');
                    print_r('<i>' . $tag . '</i>');
                  print_r('</div>');
                print_r('</li>');
              } // end foreach

            ?>

</ul> -->

<section class="media-list row">

  <div class="media-item col-12 col-sm-4">
    <img src="Content/Images/cei-video-placeholder-poster.jpg" class="img-fluid" />
      
    <h2>
      <a href="video" class="stretched-link">
        Circular Economies Institute Panel 5:
        <span class="fw-300">
          Chemical Exposures and Synthesis of Chemical Building Blocks
        </span>
      </a>
    </h2>
  </div>

  <div class="media-item col-12 col-sm-4">
    <img src="Content/Images/cei-video-placeholder-poster.jpg" class="img-fluid" />
      
    <h2>
      <a href="video" class="stretched-link">
        Circular Economies Institute Panel 4:
        <span class="fw-300">
          Sustainable Supply Chains & Battery Technology Research
        </span>
      </a>
    </h2>
  </div>

  <div class="media-item col-12 col-sm-4">
    <img src="Content/Images/cei-video-placeholder-poster.jpg" class="img-fluid" />
      
    <h2>
      <a href="video" class="stretched-link">
        Circular Economies Institute Panel 3:
        <span class="fw-300">
          Sustainable Packaging Systems & Polymer Science
        </span>
      </a>
    </h2>
  </div>

  <div class="media-item col-12 col-sm-4">
    <img src="Content/Images/cei-video-placeholder-poster.jpg" class="img-fluid" />
      
    <h2>
      <a href="video" class="stretched-link">
        Circular Economies Institute Panel 2:
        <span class="fw-300">
          Circular Economy, Transportation Systems & Mass Timber
        </span>
      </a>
    </h2>
  </div>

  <div class="media-item col-12 col-sm-4">
    <img src="Content/Images/cei-video-placeholder-poster.jpg" class="img-fluid" />
      
    <h2>
      <a href="video" class="stretched-link">
        Circular Economies Institute Panel 1:
        <span class="fw-300">
          Sustainable Supply Chains & Battery Technology Research
        </span>
      </a>
    </h2>
  </div>

  <div class="media-item col-12 col-sm-4">
    <img src="Content/Images/cei-video-placeholder-poster.jpg" class="img-fluid" />
      
    <h2>
      <a href="video" class="stretched-link">
        Small Business Technology
      </a>
    </h2>
  </div>

  
  
</section>
